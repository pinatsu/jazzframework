﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JazzFramework.Events
{
    public class BoolGameEventListener : BaseGameEventListener<bool,BoolEvent,UnityBoolEvent>
    {
    }
}
