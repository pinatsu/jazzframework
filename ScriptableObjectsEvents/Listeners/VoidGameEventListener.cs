﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JazzFramework.Events
{
    public class VoidGameEventListener : BaseGameEventListener<Void,VoidEvent,UnityVoidEvent>
    {
    }
}
