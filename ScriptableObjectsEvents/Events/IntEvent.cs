﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JazzFramework.Events
{
    [CreateAssetMenu(fileName ="New Int Event", menuName = "ScripTable Events/Int")]
    public class IntEvent : BaseGameEvent<int>{}
}
