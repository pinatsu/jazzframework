﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JazzFramework.Events
{
    [CreateAssetMenu(fileName ="New Transform Event", menuName = "ScripTable Events/Transform")]
    public class TransformEvent : BaseGameEvent<Transform>{}
}
