﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JazzFramework.Events
{
    [CreateAssetMenu(fileName ="New Game Object Event", menuName = "ScripTable Events/GameObject")]
    public class GameObjectEvent : BaseGameEvent<GameObject>{}
}
