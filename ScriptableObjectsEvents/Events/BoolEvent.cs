﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JazzFramework.Events
{
    [CreateAssetMenu(fileName ="New Bool Event", menuName = "ScripTable Events/Bool")]
    public class BoolEvent : BaseGameEvent<bool>{}
}
