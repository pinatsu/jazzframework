﻿using UnityEngine;
using UnityEngine.Events;

namespace JazzFramework.Events
{
    [System.Serializable]public class UnityBoolEvent : UnityEvent<bool>{}
}

