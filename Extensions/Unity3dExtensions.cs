﻿using System;
using UnityEngine;
using System.Collections;
using JazzFramework.Executor;
using JazzFramework.Easing;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

namespace JazzFramework.Extensions
{
    public static class Extensions
    {

        public static T[] GetColumn<T>(this T[,] array, int columnNumber)
        {
            return Enumerable.Range(0, array.GetLength(0))
                    .Select(x => array[x, columnNumber])
                    .ToArray();
        }

        public static T[] GetRow<T>(this T[,] array, int rowNumber)
        {
            return Enumerable.Range(0, array.GetLength(1))
                    .Select(x => array[rowNumber, x])
                    .ToArray();
        }

        private static System.Random rng = new System.Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static Color SetAlpha(this Color color, float alpha)
        {
            return new Color(color.r, color.g, color.b, alpha);
        }

        public static void Vec3ToColor(this ref Color color, Vector3 value)
        {
            color = new Color(value.x, value.y, value.z, color.a);
        }

        public static void Vec4ToColor(this ref Color color, Vector4 value)
        {
            color = new Color(value.x, value.y, value.z, value.w);
        }

        public static void AnimateAlpha(this CanvasGroup canvas, float to, float duration, float delay = 0f, EaseStyle ease = EaseStyle.StrongEaseOut)
        {
            string uniqueName = canvas.gameObject.GetInstanceID().ToString();
            ExecutorBase.GetManager().Destroy(uniqueName);
            float from = canvas.alpha;
            Ease _ease = EaseMethods.GetEase(ease);
            ExecEvaluation evaluator = new ExecEvaluation(duration, delay, EExecutorDirection.Forward, uniqueName);
            evaluator.SetUpdate(executor => { canvas.alpha = EaseMethods.EasedLerp(_ease, from, to, executor.Evaluation); });
            evaluator.Run();
        }
        public static void AnimateAlpha(this Graphic graphic, float to, float duration, float delay = 0f, EaseStyle ease = EaseStyle.StrongEaseOut)
        {
            string uniqueName = graphic.gameObject.GetInstanceID().ToString();
            ExecutorBase.GetManager().Destroy(uniqueName);
            float from = graphic.canvasRenderer.GetAlpha();
            Ease _ease = EaseMethods.GetEase(ease);
            ExecEvaluation evaluator = new ExecEvaluation(duration, delay, EExecutorDirection.Forward, uniqueName);
            evaluator.SetUpdate(executor => { graphic.canvasRenderer.SetAlpha(EaseMethods.EasedLerp(_ease, from, to, executor.Evaluation)); });
            evaluator.Run();
        }
        public static void AnimateColor(this Graphic graphic, Color to, float duration, float delay = 0f, EaseStyle ease = EaseStyle.StrongEaseOut)
        {
            string uniqueName = graphic.gameObject.GetInstanceID().ToString();
            ExecutorBase.GetManager().Destroy(uniqueName);
            Color from = graphic.color;
            Ease _ease = EaseMethods.GetEase(ease);
            ExecEvaluation evaluator = new ExecEvaluation(duration, delay, EExecutorDirection.Forward, uniqueName);
            evaluator.SetUpdate(executor =>
            {
                graphic.color = Color.Lerp(from, to, EaseMethods.EasedLerp(_ease, 0, 1, executor.Evaluation));
            });
            evaluator.Run();
        }

        /// <summary>
        /// Converte um dicionario para duas listas. Uma com as chaves e outra com os valores.
        /// </summary>
        /// <param name="dictionary">O dicionario</param>
        /// <typeparam name="T">O tipo da chave</typeparam>
        /// <typeparam name="U">O tipo do valor</typeparam>
        /// <returns></returns>
        public static void ToLists<T, U>(this Dictionary<T, U> dictionary, ref List<T> keys, ref List<U> values)
        {
            if (keys == null) { keys = new List<T>(); }
            else { keys.Clear(); }

            if (values == null) { values = new List<U>(); }
            else { values.Clear(); }

            foreach (var keyValuePair in dictionary)
            {
                keys.Add(keyValuePair.Key);
                values.Add(keyValuePair.Value);
            }
        }

        /// <summary>
        /// Converte duas listas, uma com as chaves e outra com os valores, para dicionario
        /// </summary>
        /// <param name="lists">As listas. Em Tuple.</param>
        /// <typeparam name="T">O Tipo da chave</typeparam>
        /// <typeparam name="U">O tipo do valor</typeparam>
        /// <returns></returns>
        public static Dictionary<T, U> ToDictionary<T, U>(this (List<T> keys, List<U> values) lists)
        {
            var dictionary = new Dictionary<T, U>();

            for (int i = 0; i < lists.keys.Count; i++)
            {
                dictionary.Add(lists.keys[i], lists.values[i]);
            }
            return dictionary;
        }
    }
}