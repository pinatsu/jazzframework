﻿using System;
using UnityEngine.Events;

public interface IPausableCallBack<T>:IPausable
{
    event Action<T> OnPauseChangeCallBack;
}
public interface IPausable
{
     bool Pause { get; set; }
}