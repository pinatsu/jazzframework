﻿using UnityEngine;
using System.Collections;

namespace JazzFramework.PoolManager
{
    public class PoolObject : MonoBehaviour
    {

        public virtual void OnSpawn()
        {

        }
        public virtual void OnDespawn()
        {

        }
    }
}
