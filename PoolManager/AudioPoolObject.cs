﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JazzFramework.PoolManager
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioPoolObject : PoolObject
    {
        private AudioSource audioSource;
        public AudioSource AudioSource { get => audioSource; }

        protected virtual void Awake()
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.playOnAwake = false;
        }
    }
}
