﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Timer : IPausable
{
    public enum ETimerDirection : int { CountDown = -1, CountUp = 1 };

    private bool pause = false;

    public bool Pause { get => pause; set => pause = value; }

    /// <summary>
    /// O tempo atual do timer em Segundos.
    /// Começa com o valor definido (Initial Timer) e decresce até 0 segundos.
    /// </summary>
    public float CurrentTimer { get => m_CurrentTime; }

    /// <summary>
    /// Retorna o valor (Current Timer) normalizado entre 0 e 1;
    /// </summary>
    public float CurrentTimeNormalized => Mathf.InverseLerp(0,m_TimerValue,m_CurrentTime);

    public Action OnTimerEnded;

    /// <summary>
    /// Tempo inicial da life bar em segundos
    /// Representa 100% da vida
    /// </summary>
    private float m_TimerValue = 1f;

    /// <summary>
    /// Tempo atual do timer em segundos.
    /// </summary>
    private float m_CurrentTime;

    /// <summary>
    /// Define se o timer irá decrescer ou crescer até o valor definido.
    /// </summary>
    private ETimerDirection m_timerDirection;

    private List<CustomElapsedTimeCallback> customElapsedTimeCallbackList = new List<CustomElapsedTimeCallback>();

    public void Update(float deltaTime)
    {
        //Retorna caso esteja pausado ou se ja tenha finalizado.
        if (pause || IsTimerEnded()) return;

        //Incrementa o timer na direção definida.
         m_CurrentTime += (deltaTime * (int)m_timerDirection);

        //Atualiza os callbacks de tempo customizados.
        for (int i = 0; i < customElapsedTimeCallbackList.Count; i++)
        {
            customElapsedTimeCallbackList[i].Update(deltaTime);
        }

        //Garante que o tempo corrente não extrapole os valores entre 0 e o tempo do timer.
        m_CurrentTime = Mathf.Clamp(m_CurrentTime, 0, m_TimerValue);

        //Checa se o tempo acabou para poder disparar o callback.
        OnTimerEndedCallBack();
    }

    /// <summary>
    /// Checa se o timer chegou ao final.
    /// </summary>
    /// <returns></returns>
    private bool IsTimerEnded()
    {

        switch (m_timerDirection)
        {
            case ETimerDirection.CountDown:
                return m_CurrentTime <= 0;
            case ETimerDirection.CountUp:
                return m_CurrentTime >= m_TimerValue;
            default:
                return true;
        }
    }

    /// <summary>
    /// Checa se o timer acabou e dispara o evento OnTimerEnded. 
    /// </summary>
    private void OnTimerEndedCallBack()
    {
        if (IsTimerEnded())
        {
            OnTimerEnded?.Invoke();
        }
    }
    public void SetTimer(float time, ETimerDirection direction)
    {
        m_timerDirection = direction;

        switch (direction)
        {
            case ETimerDirection.CountDown:
                m_CurrentTime = time;
                break;
            case ETimerDirection.CountUp:
                m_CurrentTime = 0;
                break;
        }
        m_TimerValue = time;
    }
    public void DecreaseCurrentTime(float time)
    {
        m_CurrentTime = Mathf.Clamp(m_CurrentTime - time, 0, m_TimerValue);
        OnTimerEndedCallBack();
    }
    public void IncreaseCurrentTime(float time)
    {
        m_CurrentTime = Mathf.Clamp(m_CurrentTime + time, 0, m_TimerValue);
        OnTimerEndedCallBack();
    }

    public void AddCustomElapsedTimeCallback(float time, Action callBack)
    {
        customElapsedTimeCallbackList.Add(new CustomElapsedTimeCallback(time,callBack));
    }

    internal class CustomElapsedTimeCallback
    {
        private Action m_OnElapsedTimeCallback;
        private float m_ElapsedTime;
        private float m_Time;

        public CustomElapsedTimeCallback(float time, Action callBack = null)
        {
            m_Time = time;
            if (callBack != null)
                m_OnElapsedTimeCallback += callBack;
        }
        public void Update(float deltaTime)
        {
            m_ElapsedTime += deltaTime;
            if (m_ElapsedTime >= m_Time)
            {
                m_ElapsedTime = 0;
                m_OnElapsedTimeCallback?.Invoke();
            }
        }
    }
}



