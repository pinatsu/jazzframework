﻿using UnityEngine;

namespace JazzFramework.ScriptableDataHolder
{
    [CreateAssetMenu(fileName = "New Boolean Value", menuName = "ScriptableObjects/Values/Bolean Value", order = 1)]
    public class Bolean_ScriptableObject : Value_ScriptableObjectBase<bool>
    {

    }
}



