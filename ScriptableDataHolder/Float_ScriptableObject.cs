﻿using UnityEngine;

namespace JazzFramework.ScriptableDataHolder
{
    [CreateAssetMenu(fileName = "New Float Value", menuName = "ScriptableObjects/Values/Float Value", order = 1)]
    public class Float_ScriptableObject : Value_ScriptableObjectBase<float>
    {

    }
}
