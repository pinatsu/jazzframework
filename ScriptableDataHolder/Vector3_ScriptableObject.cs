﻿using UnityEngine;

namespace JazzFramework.ScriptableDataHolder
{
    [CreateAssetMenu(fileName = "New Vector3 Value", menuName = "ScriptableObjects/Values/Vector3 Value", order = 1)]
    public class Vector3_ScriptableObject : Value_ScriptableObjectBase<Vector3>
    {

    }
}

