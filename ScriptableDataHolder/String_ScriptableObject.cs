﻿using UnityEngine;
namespace JazzFramework.ScriptableDataHolder
{
    [CreateAssetMenu(fileName = "New String Value", menuName = "ScriptableObjects/Values/String Value", order = 1)]
    public class String_ScriptableObject : Value_ScriptableObjectBase<string>
    {

    }
}


