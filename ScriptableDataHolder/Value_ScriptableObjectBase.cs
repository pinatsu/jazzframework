﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JazzFramework.ScriptableDataHolder
{
    public abstract class Value_ScriptableObjectBase<T> : ScriptableObject
    {
        [SerializeField]
        protected T value;
        public virtual T Value { get => value; }
       
    }
}


