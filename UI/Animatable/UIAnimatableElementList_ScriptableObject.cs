﻿using JazzFramework.ScriptableDataHolder;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JazzFramework.UI.Animatable
{
    [CreateAssetMenu(fileName = "UIAnimatableElementList", menuName = "ScriptableObjects/UI Animatable Element List", order = 1)]
    public class UIAnimatableElementList_ScriptableObject : ObjectListAssetBase<UIAnimatableElementBase>
    {
        public void PlayAll(bool reverse)
        {
            for (int i = 0; i < ElementsList.Count; i++)
            {
                ElementsList[i].PlayAnimation(reverse);
            }
        }

        public void PlayAllForward()
        {
            for (int i = 0; i < ElementsList.Count; i++)
            {
                ElementsList[i].PlayAnimation(false);
            }
        }

        public void PlayAllBackward()
        {
            for (int i = 0; i < ElementsList.Count; i++)
            {
                ElementsList[i].PlayAnimation(true);
            }
        }

        public void PlayAllSwitch()
        {
            for (int i = 0; i < ElementsList.Count; i++)
            {
                ElementsList[i].PlaySwitch();
            }
        }
    }
}