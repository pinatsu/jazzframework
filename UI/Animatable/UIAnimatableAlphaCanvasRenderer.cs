﻿using JazzFramework.Executor;
using UnityEngine;

namespace JazzFramework.UI.Animatable
{
    [RequireComponent(typeof(CanvasRenderer))]
    public class UIAnimatableAlphaCanvasRenderer : UIAnimatableAlphaBase
    {
        protected CanvasRenderer canvasRenderer;

        protected override void Awake()
        {
            base.Awake();
            canvasRenderer = GetComponent<CanvasRenderer>();
            originalAlphaValue = canvasRenderer.GetAlpha();
        }
        protected override void OnExecutorUpdate(ExecEvaluation executor)
        {
            base.OnExecutorUpdate(executor);
            canvasRenderer.SetAlpha(Mathf.Lerp(originalAlphaValue, targetAlphaValue, evaluation));
        }
    }
}