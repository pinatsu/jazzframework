﻿using JazzFramework.ScriptableDataHolder;
using UnityEngine;

namespace JazzFramework.UI.Animatable
{
    public abstract class UIAnimatableAlphaBase : UIAnimatableElementBase
    {
        [SerializeField] protected Float_ScriptableObject targetValueScriptObj;
        [SerializeField] protected float targetValueOffset;
        protected float targetAlphaValue;
        protected float originalAlphaValue;

        public override void PlayAnimation(bool reverse)
        {
            targetAlphaValue = targetValueOffset;
            if (targetValueScriptObj != null) { targetAlphaValue += targetValueScriptObj.Value; }
            base.PlayAnimation(reverse);
        }
    }
}
